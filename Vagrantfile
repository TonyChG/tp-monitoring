# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vm.box_check_update = true

  config.vm.define "mon" do |mon|
    mon.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 2
      vb.linked_clone = true
      vb.customize ["modifyvm", :id, "--uart1", "0x3f8", "4", "--uartmode1", "disconnected"]
    end
    mon.vm.hostname = "mon.lab.local"
    mon.vm.network "private_network", ip: "192.168.33.5"
    mon.vm.synced_folder ".", "/vagrant", type: "rsync",
      rsync__exclude: [".git/", "tools", "app", "pdns", "proxy"]

    mon.vm.provision "install", type: "shell", inline: <<-SHELL
      yum install -q -y time traceroute dialog graphviz graphviz-gd httpd libdbi pango perl-Locale-Maketext-Simple perl-IO-Zlib php php-cli php-xml php-mbstring php-pdo php-gd uuid xinetd freeradius-utils libpcap bind-utils poppler-utils libgsf rpm-build postgresql-libs ntp ntpdate nc
      yum install -q -y /vagrant/check-mk-raw-1.6.0p6-el7-38.x86_64.rpm
    SHELL

    mon.vm.provision "init", type: "shell", inline: <<-SHELL
      systemctl stop httpd.service
      omd create lab_local
      sudo -i -u lab_local omd config set APACHE_TCP_ADDR 192.168.33.5
      sudo -i -u lab_local omd config set APACHE_TCP_PORT 8081
      sudo -i -u lab_local omd config set NAGIOS_THEME exfoliation
      sudo -i -u lab_local htpasswd -mb /omd/sites/lab_local/etc/htpasswd cmkadmin cmkadmin
      sudo -i -u lab_local omd start
      mkdir /opt/backups
      chgrp omd /opt/backups
      chmod g+w /opt/backups
    SHELL

    mon.vm.provision "restart", type: "shell", run: "never", inline: <<-SHELL
      sudo -i -u lab_local omd stop || echo ""
      sleep 1
      sudo -i -u lab_local omd start || echo ""
    SHELL

    mon.vm.provision "check", type: "shell", run: "never", inline: <<-SHELL
      sudo -i -u lab_local omd status || echo ""
    SHELL
  end

  (1..2).each do |i|
    config.vm.define "lb#{i}" do |lb|
      lb.vm.provider "virtualbox" do |vb|
        vb.memory = "256"
        vb.cpus = 1
        vb.linked_clone = true
        vb.customize ["modifyvm", :id, "--uart1", "0x3f8", "4", "--uartmode1", "disconnected"]
      end
      lb.vm.hostname="lb#{i}"
      lb.vm.network "private_network", ip: "192.168.56.1#{i}"
      lb.vm.network "private_network", ip: "192.168.33.1#{i}"
      lb.vm.synced_folder ".", "/vagrant", type: "rsync",
        rsync__exclude: [".git/", "check-mk-raw-1.6.0p6-el7-38.x86_64.rpm", "tools", "app", "pdns"]
  
      lb.vm.provision "install", type: "shell", inline: <<-SHELL
        yum -q -y install epel-release
        yum -q -y install keepalived nginx telnet ntp ntpdate xinetd
      SHELL

      lb.vm.provision "init", type: "shell", run: "once", inline: <<-SHELL
        cp /etc/keepalived/keepalived.conf /etc/keepalived/keepalived.conf.orig
        cp /vagrant/proxy/keepalived.conf#{i} /etc/keepalived/keepalived.conf
        cp /vagrant/proxy/keepalived-notify#{i}.sh /usr/local/sbin/keepalived-notify.sh
        chmod +x /usr/local/sbin/keepalived-notify.sh
        setenforce 0
        sed -i 's/SELINUX=enforcing/SELINUX=permissive/' /etc/sysconfig/selinux
        cp /vagrant/proxy/nginxproxy.conf /etc/nginx/conf.d/
        systemctl enable keepalived
        systemctl start keepalived
        systemctl enable nginx
        systemctl start nginx
      SHELL
  
      lb.vm.provision "restart", type: "shell", run: "never", inline: <<-SHELL
        systemctl restart nginx
        systemctl restart keepalived
        sleep 4
        ip a | grep 192
      SHELL
  
      lb.vm.provision "check", type: "shell", run: "never", inline: <<-SHELL
        ip a | grep 192
      SHELL
    end
  end

  (1..2).each do |i|
    config.vm.define "web#{i}" do |web|
      web.vm.provider "virtualbox" do |vb|
        vb.memory = "256"
        vb.cpus = 1
        vb.linked_clone = true
      end
      web.vm.hostname = "web#{i}.lab.local"
      web.vm.network "private_network", ip: "192.168.33.20#{i}"
      web.vm.synced_folder ".", "/vagrant", type: "rsync",
        rsync__exclude: [".git/", "check-mk-raw-1.6.0p6-el7-38.x86_64.rpm", "tools", "pdns", "proxy"]

     web.vm.provision "install", type: "shell", run: "once", inline: <<-EOF
       yum -q -y install ntp ntpdate xinetd telnet
       /vagrant/app/task/init.sh
     EOF

      web.vm.provision "init", type: "shell", run: "once", inline: <<-EOF
        sed -i 's/localhost/192.168.33.20#{i}/g' /vagrant/app/src/testapp.js
        /vagrant/app/task/apply.sh
      EOF

      web.vm.provision "restart", type: "shell", run: "never", inline: <<-EOF
        sed -i 's/localhost/192.168.33.20#{i}/g' /vagrant/app/src/testapp.js
        /vagrant/app/task/apply.sh
      EOF

      web.vm.provision "check", type: "shell", run: "never", inline: <<-EOF
        ss -anptl | grep 9000 || echo "no listener on port 9000"
      EOF
    end
  end

  config.vm.define "dns" do |dns|
    dns.vm.provider "virtualbox" do |vb|
      vb.memory = "256"
      vb.cpus = 1
      vb.linked_clone = true
      vb.customize ["modifyvm", :id, "--uart1", "0x3f8", "4", "--uartmode1", "disconnected"]
    end
    dns.vm.hostname = "dns"
    dns.vm.network "private_network", ip: "192.168.33.2"
    dns.vm.synced_folder ".", "/vagrant", type: "rsync",
      rsync__exclude: [".git/", "check-mk-raw-1.6.0p6-el7-38.x86_64.rpm", "tools", "app", "proxy"]

    dns.vm.provision "install", type: "shell", run: "once", inline: <<-EOF
      yum -y -q install epel-release
      yum -y -q install pdns-recursor pdns pdns-backend-mysql mariadb-server httpd php php-mcrypt php-mysql telnet bind-utils xinetd ntp ntpdate
      curl -o /opt/poweradmin-2.1.7.tgz https://freefr.dl.sourceforge.net/project/poweradmin/poweradmin-2.1.7.tgz
      tar zxf /opt/poweradmin-2.1.7.tgz -C /var/www/html/
      ln -s /var/www/html/poweradmin-2.1.7 /var/www/html/poweradmin
    EOF

    dns.vm.provision "init", type: "shell", run: "once", inline: <<-EOF
      cp /etc/pdns-recursor/recursor.conf /etc/pdns-recursor/recursor.conf.orig
      cp /etc/pdns/pdns.conf /etc/pdns/pdns.conf.orig
      cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.orig
      cp /vagrant/pdns/recursor.conf /etc/pdns-recursor/recursor.conf
      cp /vagrant/pdns/pdns.conf /etc/pdns/pdns.conf
      cp /vagrant/pdns/httpd.conf /etc/httpd/conf/httpd.conf
      grep -q "# vagrantedit" || sed -i.orig -e '3a# vagrantedit' -e '3abind-address=127.0.0.1' /etc/my.cnf
      cp /vagrant/pdns/poweradmin-config.inc.php /var/www/html/poweradmin/inc/config.inc.php
      mv /var/www/html/poweradmin/install /var/www/html/poweradmin/install-moved
      systemctl enable mariadb.service
      systemctl start mariadb.service
      mysql <<EOF2
create database pdns;
GRANT SELECT, UPDATE, INSERT, DELETE ON pdns.* TO 'pdns'@'localhost' IDENTIFIED BY 'zzzzzz';
GRANT SELECT, UPDATE, INSERT, DELETE ON pdns.* TO 'padmin'@'localhost' IDENTIFIED BY 'zzzzzz';
FLUSH PRIVILEGES;
use pdns;
source /vagrant/pdns/pdns.sql;
EOF2
      systemctl enable httpd.service
      systemctl start httpd.service
      systemctl enable pdns.service
      systemctl start pdns.service
      systemctl enable pdns-recursor.service
      systemctl start pdns-recursor.service
      systemctl stop firewalld.service
    EOF

    dns.vm.provision "restart", type: "shell", run: "never", inline: <<-EOF
      systemctl restart pdns-recursor.service
      systemctl restart httpd.service
      systemctl restart mariadb.service
      systemctl restart pdns.service
    EOF

    dns.vm.provision "check", type: "shell", run: "never", inline: <<-EOF
      systemctl -n 0 -o short status mariadb.service httpd.service pdns-recursor.service pdns.service
    EOF
  end

  config.vm.provision "initmonit", type: "shell", run: "never", inline: <<-EOF
    systemctl stop NetworkManager.service
    grep -q 'dns=none' /etc/NetworkManager/NetworkManager.conf || sed -i.orig \'s!^\\[main\\]!\\[main\\]\\ndns=none!\' /etc/NetworkManager/NetworkManager.conf
    grep -q 192.168.33.2 /etc/resolv.conf || echo nameserver 192.168.33.2 > /etc/resolv.conf
    yum -y -q install http://192.168.33.5:8081/lab_local//check_mk/agents/check-mk-agent-1.6.0p6-1.noarch.rpm && echo done
    sed -i 's/#only_from.*$/only_from      = 192.168.33.5 127.0.0.1/' /etc/xinetd.d/check_mk
    systemctl restart xinetd
    systemctl start NetworkManager.service
  EOF
  config.vm.provision "initsnmp", type: "shell", run: "never", inline: <<-EOF
    yum -y -q install net-snmp
    sed -i.orig 's/.1.3.6.1.2.1.1/.1.3.6.1.2.1/' /etc/snmp/snmpd.conf
    systemctl enable snmpd.service
    systemctl start snmpd.service
  EOF
end
