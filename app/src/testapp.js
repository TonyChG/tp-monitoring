var http = require('http');
var os = require('os');

const msg = "Hello from " + os.hostname() + "\n";

// Configure HTTP server respond with Hello World to all requests
var server = http.createServer(function (request, response) {
     response.writeHead(200, {"Content-Type": "text/plain"});
     response.end(msg);
     });

server.listen(9000, 'localhost');

console.log("Server running at http://localhost:9000/");

