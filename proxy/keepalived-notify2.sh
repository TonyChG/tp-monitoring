#!/bin/bash
TYPE=$1
NAME=$2
STATE=$3
case $STATE in
        "BACKUP") echo "0 Vip-$TYPE-$NAME - OK: $STATE" > /var/lib/check_mk_agent/spool/Vip-$TYPE-$NAME.out
                  ;;
        "MASTER") echo "1 Vip-$TYPE-$NAME - Warning: $STATE" > /var/lib/check_mk_agent/spool/Vip-$TYPE-$NAME.out
                  ;;
        "FAULT")  echo "2 Vip-$TYPE-$NAME - Critical: $STATE" > /var/lib/check_mk_agent/spool/Vip-$TYPE-$NAME.out
                  exit 0
                  ;;
        *)        /sbin/logger "Vip unknown state"
                  exit 1
                  ;;
esac
